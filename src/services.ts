import { Security } from "./services/Security";
import { Search } from "./services/Search";
import { Dispatch } from "redux";
import {
  searchStart,
  searchSuccess,
  searchFailed
} from "./reducers/search.reducer";

export const security = new Security();

export const search = new Search(security);

export const searchAlbums = (dispatch: Dispatch) => (query: string) => {
  dispatch(searchStart(query));

  search.search(query).then(
    results => {
      dispatch(searchSuccess(results));
    },
    error => {
      dispatch(searchFailed(error));
    }
  );
};
