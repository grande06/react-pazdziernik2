import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import { security } from "./services";
import { SearchProvider } from "./services/SearchContext";
import { store } from "./store";
import { Provider } from "react-redux";

security.getToken();
store;

ReactDOM.render(
  <Provider store={store}>
    <SearchProvider>
      <App />
    </SearchProvider>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();

// interface IWindow {
//   [k: string]: any;
// }

// (window as IWindow)["React"] = React;
// (window as IWindow)["ReactDOM"] = ReactDOM;
