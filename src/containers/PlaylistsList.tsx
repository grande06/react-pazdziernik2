import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { AppState } from "src/store";
import { ItemsList, Item } from "../components/ItemsList";
import { selectPlaylist } from "../reducers/playlists.reducer";
import { Dispatch } from "redux";
import { FilterList } from '../components/FilterList';

type Props = {
  items: Item[];
  selected?: Item;
};
type DispatchProps = {
  onSelect(item: Item): void;
};

const mapStateToProps: MapStateToProps<Props, {}, AppState> = state => ({
  items: state.playlists.playlists,
  selected: state.playlists.playlists.find(
    playlist => playlist.id === state.playlists.selectedId
  )
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
  dispatch: Dispatch
) => ({
  onSelect: (item: Item) => {
    dispatch(selectPlaylist(item.id));
  }
});

const withPlaylist = connect(
  mapStateToProps,
  mapDispatchToProps
);

export const PlaylistList = withPlaylist(ItemsList);
export const FilterPlaylistList = withPlaylist(FilterList);
