import * as React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistsView } from "./views/PlaylistsView";
import { MusicSearch } from "./views/MusicSearch";

class App extends React.Component {
  state = {
    tab: "playlists"
  };

  select = (tab: string) => {
    this.setState({ tab });
  };

  public render() {
    return (
      <>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <a className="navbar-brand" href="#">
              Music App
            </a>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link" onClick={()=>this.select('playlists')}>
                    Playlists
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" onClick={()=>this.select('search')}>
                    Search
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col">
              {this.state.tab == "playlists" && <PlaylistsView />}
              {this.state.tab == "search" && <MusicSearch />}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
