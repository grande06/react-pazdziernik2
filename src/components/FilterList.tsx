import * as React from "react";
import { SearchField } from "./SearchField";
import { ItemsList, Item, Props } from "./ItemsList";

type State = {
  query: string;
};

export class FilterList extends React.PureComponent<Props, State> {
  state: State = {
    query: ""
  };

  search = (query: string) => {
    this.setState({
      query
    });
  };

  filterItems(items: Item[], query: string) {
    console.log("filter");
    return items.filter(item =>
      item.name.toLowerCase().includes(query.toLowerCase())
    );
  }

  render() {
    const filteredItems = this.filterItems(this.props.items, this.state.query);

    return (
      <div>
        <SearchField onSearch={this.search} />
        <ItemsList {...this.props} items={filteredItems} />
      </div>
    );
  }
}

// newProps = {
//   ...oldProps,
//   items: changedItems
// }