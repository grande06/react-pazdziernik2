import { createStore, combineReducers } from "redux";
import { counter } from "./reducers/counter.reducer";
import { PlaylistsState, playlists } from "./reducers/playlists.reducer";
import { SearchState, search } from "./reducers/search.reducer";

export type AppState = {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
};

const reducer = combineReducers<AppState>({
  counter,
  playlists,
  search
});

// ==

export const store = createStore(
  reducer,
  window["__REDUX_DEVTOOLS_EXTENSION__"] && window["__REDUX_DEVTOOLS_EXTENSION__"]()
);

window["store"] = store;

// store.subscribe(() => {
//   console.log("NEW State:", store.getState());
// });
