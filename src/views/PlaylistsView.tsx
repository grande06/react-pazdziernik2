import * as React from "react";
import { PlaylistList } from "src/containers/PlaylistsList";
import {
  SelectedPlaylistDetails,
  SelectedPlaylistForm
} from "../containers/Playlist";

type State = {
  editing: boolean;
};

export class PlaylistsView extends React.Component<{}, State> {
  state: State = {
    editing: false
  };

  edit = () => {
    this.setState({
      editing: true
    });
  };

  cancel = () => {
    this.setState({
      editing: false
    });
  };

  newPlaylist = () => {
    this.setState({ editing: true });
  };

  save = () => {
    this.setState({
      editing: false
    });
  };

  render() {
    return (
      <div className="row">
        <div className="col">
          <PlaylistList />
          <input
            type="button"
            value="New Playlist"
            className="btn btn-info mt-2 float-right"
            onClick={this.newPlaylist}
          />
        </div>
        <div className="col">
          {this.state.editing ? (
            <SelectedPlaylistForm onCancel={this.cancel} onSave={this.save} />
          ) : (
            <SelectedPlaylistDetails onEdit={this.edit} />
          )}
        </div>
      </div>
    );
  }
}

// export default PlaylistsView;
