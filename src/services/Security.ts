export class Security {
  constructor(
    private api_url = "https://accounts.spotify.com/authorize",
    private client_id = "a5a521018d424374b2c5c068f361149d",
    private response_type = "token",
    private redirect_uri = "http://localhost:3000/"
  ) {}

  authorize() {
    const url = `${this.api_url}?client_id=${this.client_id}&response_type=${
      this.response_type
    }&redirect_uri=${this.redirect_uri}`;

    location.replace(url);
  }

  token = "";

  getToken() {
    if (location.hash && !this.token) {
      const match = location.hash.match(/access_token=([^&]*)/);
      this.token = (match && match[1]) || "";
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
